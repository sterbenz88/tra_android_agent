package com.thingscare.tra.web;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import com.thingscare.tra.data.MeshInfo;
import com.thingscare.tra.data.RegisterInfo;
import com.thingscare.tra.protocol.Protocol;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;
import com.thingscare.tra.util.TraJson;
import com.thingscare.tra.util.TraProperties;

import java.nio.charset.Charset;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;


public class WebSession {

    private static final int    WEB_FAILURE = 0;
    private static final int    WEB_SUCCESS = 1;
    private static final int    WEB_FORCE_OUT = 2;

    private WebSessionListener mListener;
    private Context mContext;

    private TraProperties traProperties;

    private String serverAddress;
    private OkHttpClient clientWebSocket;
    private Request requestWebSocket;
    private WebSocketListener webSocketListener;
    private static WebSocket myWebSocket;

    private long retryInterval = 3000;
    private Handler retryHandler = new Handler();
    private Runnable retryRunnable = new Runnable() {
        @Override
        public void run() {

            runWebSocket(serverAddress);
        }
    };

    public WebSession(Context context) {
        mContext				= context.getApplicationContext();
        traProperties           = TraProperties.getInstance();
        myWebSocket               = null;
    }

    //------------------------------------------------------------------
    //	Listener
    //------------------------------------------------------------------
    public void setListener(WebSessionListener listener) {
        this.mListener = listener;
    }

    public void deleteListener() {
        this.mListener = null;
    }

    public void notifyListener(short Command) {
        if(mListener != null) {
            mListener.updateWebSession(Command, null);
        }
    }

    public void notifyListener(short Command, Message msg) {
        if(mListener != null) {
            mListener.updateWebSession(Command, msg);
        }
    }

    public void retryCancel() {
        retryHandler.removeCallbacks(retryRunnable);
    }

    public void close() {
        try {

            if (myWebSocket != null) {
                myWebSocket.close(1000, null);
                myWebSocket.cancel();
                myWebSocket = null;
            }

        }catch (Exception e) {
            TraLog.d(Constants.TAG.PARK, "Websocket close1 : " + e.getMessage());
        }

        try {

            clientWebSocket = null;
            requestWebSocket = null;

        }catch (Exception e) {
            TraLog.d(Constants.TAG.PARK, "Websocket close2 : " + e.getMessage());
        }
    }

    public void runWebSocket(final String url) {

        this.serverAddress = url;
        TraLog.d(Constants.TAG.PARK, "runWebSocket : " + url);

        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            clientWebSocket = new OkHttpClient().newBuilder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .retryOnConnectionFailure(true)
                    .build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        requestWebSocket = new Request.Builder().url(url).build();

        webSocketListener = new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {

                try {
                    String sendMessage = TraJson.generateActionMashes().toString();
                    webSocket.send(sendMessage);
                    TraLog.e(Constants.TAG.PARK, "onOpen : " + sendMessage);

                }catch (Exception e) {
                    TraLog.e(Constants.TAG.PARK, "onOpen exception : " + e.getMessage());
                    notifyListener(Protocol.MSG_TRA_WEB_CONNECT_FAILURE);
                }
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                TraLog.e(Constants.TAG.PARK, "MESSAGE1: " + text);
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {

                String onMessageString;
                try {
                    onMessageString = bytes.string(Charset.defaultCharset());
                    TraLog.e(Constants.TAG.PARK, "MESSAGE2: " + onMessageString);

                    short command = TraJson.parsingMessageType(onMessageString);
                    switch (command) {

                        case Protocol.SERVER_MESH_INFO:

                            MeshInfo parsingMeshInfoData = TraJson.parsingMeshInfo(onMessageString);
                            if (parsingMeshInfoData != null) {
                                traProperties.setMeshInfo(parsingMeshInfoData);

                                String nodeid = null;
                                if (traProperties.getIsAutoRun())
                                    nodeid = traProperties.getNodeid();

                                String registerMessage = TraJson.generateActionRegister(parsingMeshInfoData.getMeshid(),
                                                                                        1,
                                                                                        3,
                                                                                        0,
                                                                                        2,
                                                                                        56,
                                                                                        Build.MODEL,
                                                                                        nodeid).toString();

                                webSocket.send(registerMessage);

                                TraLog.d(Constants.TAG.PARK, "SEND: " + registerMessage);

                            } else {
                                // Reset and connection retry !!!
                            }
                            break;

                        case Protocol.SERVER_REGISTER_INFO:

                            RegisterInfo parsingRegisterInfoData = TraJson.parsingRegisterInfo(onMessageString);
                            if (parsingRegisterInfoData != null) {
                                traProperties.setRegisterInfo(parsingRegisterInfoData);

                                notifyListener(Protocol.MSG_TRA_WEB_CONNECTED);
                                myWebSocket = webSocket;

                                String coreinfoMessage = TraJson.generateActionCoreinfo("MeshCore v6",
                                                                                        15).toString();

                                webSocket.send(coreinfoMessage);

                                TraLog.d(Constants.TAG.PARK, "SEND: " + coreinfoMessage);

                            } else {
                                // Reset and connection retry !!!
                            }
                            break;

                        default:

                            TraLog.d(Constants.TAG.PARK, "Unknown command : " + command);
                            break;
                    }

                }catch (Exception e) {
                    TraLog.e(Constants.TAG.PARK, "MESSAGE2 exception : " + e.getMessage());
                    notifyListener(Protocol.MSG_TRA_WEB_DISCONNECTED);
                }
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {
                webSocket.close(1000, null);
                webSocket.cancel();

                myWebSocket = null;
                TraLog.e(Constants.TAG.PARK, "onClosing: " + code + " " + reason);
            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {

                TraLog.e(Constants.TAG.PARK, "onClosed");
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {

                TraLog.e(Constants.TAG.PARK, "onFailure : " + t.getMessage());

                myWebSocket = null;
                if (traProperties.getIsAutoRun()) {

                    retryHandler.postDelayed(retryRunnable, retryInterval);
                }
                else {

                    notifyListener(Protocol.MSG_TRA_WEB_DISCONNECTED);
                }
            }
        };

        clientWebSocket.newWebSocket(requestWebSocket, webSocketListener);
        clientWebSocket.dispatcher().executorService().shutdown();
    }
}