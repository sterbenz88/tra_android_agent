package com.thingscare.tra.web;

import android.os.Message;

public interface WebSessionListener {
    public void updateWebSession(short Command, Message msg);
}
