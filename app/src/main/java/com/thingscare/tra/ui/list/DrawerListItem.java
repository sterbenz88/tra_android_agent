package com.thingscare.tra.ui.list;

public class DrawerListItem {

    public int Type;

    private String deviceAlias;
    private String userGroup;

    private String defaultContent;

    public DrawerListItem() {}

    /// Getter
    public int getType() {
        return Type;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public String getDefaultContent() {
        return defaultContent;
    }

    /// Setter
    public void setType(int type) {
        Type = type;
    }

    public void setDeviceAlias(String deviceAlias) {
        this.deviceAlias = deviceAlias;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public void setDefaultContent(String defaultContent) {
        this.defaultContent = defaultContent;
    }
}
