package com.thingscare.tra.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thingscare.tra.R;
import com.thingscare.tra.ui.MainActivity;
import com.thingscare.tra.ui.progress.ApiProgress;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraPreference;
import com.thingscare.tra.util.TraProperties;

public class LoginFragment extends Fragment implements View.OnClickListener,
                                                            View.OnFocusChangeListener {

    private LoginFragmentListener 	mMainActivity;
    private Context mContext;

    private TraPreference traPreference;
    private TraProperties traProperties;

    private TextView inputPathView;
    private TextView findBtn;
    private TextView loginBtn;

    //private TextWatcher idTextWatcherInput;
    //private TextWatcher pwTextWatcherInput;
    //private TextWatcher serverTextWatcherInput;

    private ApiProgress apiProgress;
    private ApiProgress autoRunWaitProgress;

    private String serverAddress;

    public interface LoginFragmentListener {

        void onReqTraLogin(String serverAddress);
        void onReqTraLoginCancel();

        void onReqChangeFragment(MainActivity.FragmentID fragmentId);

        void onReqFileChooser();
        //void onReqFileExplorer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.screen_login, container, false);

        inputPathView = (TextView) view.findViewById(R.id.inputPathPw);
        findBtn = (TextView) view.findViewById(R.id.findTw);

        loginBtn = (TextView) view.findViewById(R.id.loginBtn);
        //serverEdit = (EditText) view.findViewById(R.id.serverEdit);
        //serverEditGuide = (TextView) view.findViewById(R.id.serverEditGuide);

        apiProgress = new ApiProgress(getActivity(), getResources().getString(R.string.default_text), View.GONE);
        autoRunWaitProgress = new ApiProgress(getActivity(),getResources().getString(R.string.default_autorun_wait_text) , View.VISIBLE);
        autoRunWaitProgress.setCancelButtonListener(this);

        traPreference = new TraPreference(getActivity());
        traProperties = TraProperties.getInstance();

        serverAddress = traPreference.getValue(TraPreference.PREF_SERVER_ADDRESS, Constants.DEFAULT.SERVER_ADDRESS);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mMainActivity = (LoginFragmentListener) getActivity();
        mContext = getActivity();

        loginBtn.setOnClickListener(this);
        findBtn.setOnClickListener(this);

        //initTextWatcher();
        //serverEdit.addTextChangedListener(serverTextWatcherInput);

        //serverEdit.setText(serverAddress);
    }

    @Override
    public void onResume() {
        super.onResume();

        boolean sharedValueRememberAccount = traPreference.getValue(TraPreference.PREF_AUTO_LOGIN, false);
        //String savedNodeID = traPreference.getValue(TraPreference.PREF_NODE_ID, "");

        if (traProperties.getPassSplash() && sharedValueRememberAccount && !traProperties.getIsAutoRun()) {

            // Auto login start.
            if (apiProgress != null) {
                apiProgress.show();
            }

            mMainActivity.onReqTraLogin(serverAddress);

        } else if (sharedValueRememberAccount) {

            // Already auto login try.
            if (autoRunWaitProgress != null)
                autoRunWaitProgress.show();
        }

        traProperties.setPassSplash(false);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public void onClick(View v) {

        try {

            if (v == null)
                return;

            switch (v.getId()) {

                case R.id.api_cancelButton:

                    if (autoRunWaitProgress != null) {
                        autoRunWaitProgress.dismiss();
                    }

                    mMainActivity.onReqTraLoginCancel();
                    break;

                case R.id.loginBtn:

                    if (traProperties.getIsAutoRun()) {
                        if (autoRunWaitProgress != null) {
                            autoRunWaitProgress.show();
                        }
                    } else {
                        if (apiProgress != null) {
                            apiProgress.show();
                        }
                    }

                    //mMainActivity.onReqTraLogin(serverEdit.getText().toString());
                    break;

                case R.id.findTw:

                    mMainActivity.onReqFileChooser();
                    break;
            }


        }catch (Exception e) {}
    }

    public void onLoginResult(boolean isSuccess) {

        if (autoRunWaitProgress != null)
            autoRunWaitProgress.dismiss();

        if (apiProgress != null)
            apiProgress.dismiss();

        if (autoRunWaitProgress != null)
            autoRunWaitProgress.dismiss();

        if  (isSuccess) {

            //traPreference.put(TraPreference.PREF_SERVER_ADDRESS, serverEdit.getText().toString());

            mMainActivity.onReqChangeFragment(MainActivity.FragmentID.FRAGMENT_MAIN);
        }
    }

    /*
    private void initTextWatcher() {

        idTextWatcherInput = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        pwTextWatcherInput = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        serverTextWatcherInput = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }
    */
}
