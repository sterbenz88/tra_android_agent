package com.thingscare.tra.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.thingscare.tra.R;
import com.thingscare.tra.util.Constants;

import java.util.ArrayList;

public class DrawerListAdapter extends BaseAdapter implements View.OnClickListener {

    private DrawerAdapterListener mMainActivity;

    private ArrayList<DrawerListItem> drawerData;
    private LayoutInflater inflater;

    // Header
    private TextView m_DrawerListHeaderAliasTw;
    private TextView m_DrawerListHeaderGroupTw;
    private ImageView m_DrawerListHeaderClose;
    private FrameLayout m_DrawHeaderLayout;

    // About
    private TextView m_DrawerAboutContentTw;
    private ImageView m_DrawerAboutArrow;
    private FrameLayout m_DrawerAboutLayout;

    // Logout
    private TextView m_DrawerLogoutContentTw;
    private FrameLayout m_DrawerLogoutLayout;

    public interface DrawerAdapterListener {
        void onClickHeaderClose();
        void onClickAbout();
        void onClickLogout();
    }

    public DrawerListAdapter(Context context, ArrayList<DrawerListItem> drawerData) {
        if( context == null ) return;

        mMainActivity = (DrawerAdapterListener)context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if( drawerData != null )
            this.drawerData = drawerData;
    }

    @Override
    public int getCount() {
        return drawerData.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void onClick(View v) {
        try {

            if (v == null) {
                return;
            }

            int res;
            switch (v.getId()) {

                case R.id.drawHeaderClose:
                    mMainActivity.onClickHeaderClose();
                    break;

                case R.id.drawLogoutContent:
                    mMainActivity.onClickLogout();
                    break;

                case R.id.drawDefaultLayout:

                    res = (int)v.getTag();
                    switch (res) {
                        case Constants.DRAWER.LIST_TYPE_ABOUT:
                            mMainActivity.onClickAbout();
                            break;
                    }
                    break;
            }

        }catch (Exception e) {}
    }

    public int getItemViewType(int position){
        return drawerData.get(position).Type;
    }

    public void setHeaderUserGroup(String group) {
        drawerData.get(Constants.DRAWER.LIST_TYPE_HEADER).setUserGroup(group);
    }

    public void setHeaderAlias(String alias) {
        drawerData.get(Constants.DRAWER.LIST_TYPE_HEADER).setDeviceAlias(alias);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int res = 0;

        res = getItemViewType(position);
        switch(res){
            case Constants.DRAWER.LIST_TYPE_HEADER:
                res = R.layout.drawer_header;
                break;
            case Constants.DRAWER.LIST_TYPE_ABOUT:
                res = R.layout.drawer_default;
                break;
            case Constants.DRAWER.LIST_TYPE_LOGOUT:
                res = R.layout.drawer_logout;
                break;
        }

        convertView = inflater.inflate(res, parent, false);

        if( convertView == null ) return null;

        res = getItemViewType(position);

        switch(res) {
            case Constants.DRAWER.LIST_TYPE_HEADER:

                m_DrawHeaderLayout = (FrameLayout)convertView.findViewById(R.id.drawHeaderLayout);
                m_DrawerListHeaderGroupTw = (TextView)convertView.findViewById(R.id.drawHeaderGroup);
                m_DrawerListHeaderAliasTw = (TextView)convertView.findViewById(R.id.drawHeaderAlias);
                m_DrawerListHeaderClose = (ImageView)convertView.findViewById(R.id.drawHeaderClose);
                m_DrawerListHeaderClose.setOnClickListener(this);

                m_DrawerListHeaderGroupTw.setText(drawerData.get(position).getUserGroup());
                m_DrawerListHeaderAliasTw.setText(drawerData.get(position).getDeviceAlias());
                break;

            case Constants.DRAWER.LIST_TYPE_ABOUT:

                m_DrawerAboutLayout = (FrameLayout) convertView.findViewById(R.id.drawDefaultLayout);
                m_DrawerAboutContentTw = (TextView) convertView.findViewById(R.id.drawDefaultContent);

                m_DrawerAboutContentTw.setText(drawerData.get(position).getDefaultContent());
                m_DrawerAboutLayout.setOnClickListener(this);
                m_DrawerAboutLayout.setTag(res);
                break;

            case Constants.DRAWER.LIST_TYPE_LOGOUT:

                m_DrawerLogoutContentTw = (TextView)convertView.findViewById(R.id.drawLogoutContent);
                m_DrawerLogoutContentTw.setOnClickListener(this);
                break;
        }

        return convertView;
    }

}