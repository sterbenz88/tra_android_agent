package com.thingscare.tra.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.thingscare.tra.R;
import com.thingscare.tra.ui.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainFragment extends Fragment implements View.OnClickListener {

    private MainFragmentListener 	mMainActivity;
    private Context mContext;

    private ImageView optionBtn;

    public interface MainFragmentListener {
        void onReqChangeFragment(MainActivity.FragmentID fragmentId);
        void closeDrawerLayout();
        void openDrawerLayout();
        void disableDrawerLayout();
        void enableDrawerLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_main, container, false);

        mContext = getActivity();
        mMainActivity = (MainFragmentListener)mContext;

        optionBtn = (ImageView) view.findViewById(R.id.optionBtn);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        optionBtn.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onBackPressed() {
        getActivity().moveTaskToBack(true);
    }

    @Override
    public void onClick(View v) {

        try {

            if (v == null)
                return;

            switch (v.getId()) {

                case R.id.optionBtn:

                    mMainActivity.openDrawerLayout();
                    break;
            }


        }catch (Exception e) {}
    }
}
