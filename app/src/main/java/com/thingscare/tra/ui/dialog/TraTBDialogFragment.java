package com.thingscare.tra.ui.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.thingscare.tra.R;

public class TraTBDialogFragment extends DialogFragment {

    private View.OnClickListener buttonOkListener;
    private View.OnClickListener buttonCancelListener;
    private Button ok;
    private Button cancel;
    private TextView view_comment;
    private TextView view_title;
    private String m_comment = "";
    private String m_title = "";

    public TraTBDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.tra_dialog_double_button, container);

        view_comment = (TextView) view.findViewById(R.id.comment);
        view_title = (TextView) view.findViewById(R.id.title);
        ok = (Button)view.findViewById(R.id.ok_twobutton);
        cancel = (Button)view.findViewById(R.id.cancel_twobutton);

        ok.setOnClickListener(buttonOkListener);
        cancel.setOnClickListener(buttonCancelListener);

        if ( !m_title.equals("") )
            view_title.setText(m_title);

        if ( !m_comment.equals("") )
            view_comment.setText(m_comment);

        return view;
    }

    public void setTitleText(String title) {
        m_title = title;
    }

    public void setMessageText(String msg) {
        m_comment = msg;
    }

    public void setOkOnClickListener(View.OnClickListener listener) {
        buttonOkListener = listener;
    }
    public void setCancelOnClickListener(View.OnClickListener listener) {
        buttonCancelListener = listener;
    }

}
