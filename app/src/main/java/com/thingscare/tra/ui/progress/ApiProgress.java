package com.thingscare.tra.ui.progress;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.thingscare.tra.R;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;

public class ApiProgress extends Dialog {

    private TextView progress_title;
    private Button cancelButton;
    private View.OnClickListener cancelButtonListener;

    private String tMag;
    private int cancel_state;

    public ApiProgress(Context context, String msg, int cancel_state) {
        super(context);
        tMag = msg;
        this.cancel_state = cancel_state;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        TraLog.d(Constants.TAG.PARK, "ApiProgress onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_api);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress_title = (TextView) findViewById(R.id.api_progress_title);
        cancelButton = (Button) findViewById(R.id.api_cancelButton);
        cancelButton.setOnClickListener(cancelButtonListener);
        cancelButton.setVisibility(cancel_state);

        progress_title.setText(tMag);

        this.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        // Do nothing.
        // super.onBackPressed();
    }

    public void setCancelButtonListener(View.OnClickListener listener) {
        cancelButtonListener = listener;
    }

    public void setCancelButtonVisible(int visible_state) {
        try {
            if( cancelButton != null ) {
                cancelButton.setVisibility(visible_state);
            }
        }catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "ApiProgress exception : " + e.getMessage());
        }
    }
}
