package com.thingscare.tra.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ListView;


import com.thingscare.tra.R;
import com.thingscare.tra.protocol.Protocol;
import com.thingscare.tra.service.TraMainService;
import com.thingscare.tra.ui.fragment.LoginFragment;
import com.thingscare.tra.ui.fragment.MainFragment;
import com.thingscare.tra.ui.fragment.SplashFragment;
import com.thingscare.tra.ui.list.DrawerListAdapter;
import com.thingscare.tra.ui.list.DrawerListItem;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;
import com.thingscare.tra.util.TraProperties;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SplashFragment.SplashFragmentListener,
                                                                LoginFragment.LoginFragmentListener,
                                                                MainFragment.MainFragmentListener,
                                                                DrawerListAdapter.DrawerAdapterListener {

    private Context mContext;

    private SplashFragment mSplashFragment;
    private LoginFragment mLoginFragment;
    private MainFragment mMainFragment;

    private FragmentID  mCurrentFragmentIndex;
    private FragmentID	 mPreviousFragmentIndex;

    private TraProperties traProperties;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////  Interface
    ////////////////////////////////////////////////////////////////////////////////////////////////
    private Messenger mServiceMessenger;
    private Messenger	mMainMessenger;
    private TraServiceConnection  mTraServiceConnection;
    private MainMessageHandler	mMainHandler;

    public enum FragmentID {
        FRAGMENT_SPLASH, FRAGMENT_LOGIN, FRAGMENT_MAIN
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_base);

        mContext = this;

        mSplashFragment = new SplashFragment();
        mLoginFragment = new LoginFragment();
        mMainFragment = new MainFragment();

        initInstance();

        FragmentID fragmentID;

        if (traProperties.getServerConn()) {
            fragmentID = FragmentID.values()[FragmentID.FRAGMENT_MAIN.ordinal()];
        }else if (traProperties.getIsAutoRun()) {
            fragmentID = FragmentID.values()[FragmentID.FRAGMENT_LOGIN.ordinal()];
        } else {
            fragmentID = FragmentID.values()[FragmentID.FRAGMENT_SPLASH.ordinal()];
        }

        mPreviousFragmentIndex = fragmentID;
        mCurrentFragmentIndex = fragmentID;
        changeFragment(fragmentID);

        mTraServiceConnection = new TraServiceConnection();
        mMainHandler = new MainMessageHandler(this);
        mMainMessenger = new Messenger(mMainHandler);

        startTraService();
        bindTraService();

        initDrawerSetting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        traProperties.setIsActivityRun(false);

        unbindTraService();
        stopTraService();
    }

    public void initInstance() {
        traProperties           = TraProperties.getInstance();
        traProperties.setIsActivityRun(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        TraLog.d(Constants.TAG.PARK, String.format("onActivityResult requestCode[%d], resultCode[%d]",
                                                    requestCode, resultCode));
        /*
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    importFile = new File(uri.getPath());
                    String sFilePath = importFile.getAbsolutePath();
                    if (uri.getPath() != null) {
                        edImportPath.setText(uri.getPath());
                    }

                    Log.d("File uri", "File Uri: " + uri.toString());
                    // Get the path
                    try {
                        String path = getPath(this, uri);
                        Log.d("File Path", "File Path: " + path);
                        // Get the file instance
                        // File file = new File(path);
                        // Initiate the upload
                    } catch (URISyntaxException use) {
                        use.printStackTrace();
                    }
                }
                break;
        }
        */
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onReqChangeFragment(FragmentID fragmentId) {

        changeFragment(fragmentId);
    }

    // LoginFragment //
    @Override
    public void onReqTraLogin(String serverAddress) {

        Message message = Message.obtain(null, Protocol.MSG_TRA_WEB_CONNECT);

        Bundle mBle = new Bundle();

        mBle.putString("server", serverAddress);
        message.setData(mBle);

        sendToServiceMessenger(message);
    }

    @Override
    public void onReqTraLoginCancel() {

        Message message = Message.obtain(null, Protocol.MSG_TRA_WEB_CONNECT_CANCEL);
        sendToServiceMessenger(message);
    }

    @Override
    public void onReqFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                + "/Download/");
        intent.setDataAndType(uri, "*/*");
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.login_file_chooser_title)));
    }

    // MainFragment //
    @Override
    public void closeDrawerLayout() {

        if( dlDrawer != null ) {
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void openDrawerLayout() {

        if( dlDrawer != null ) {
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void disableDrawerLayout() {

        if( dlDrawer != null )
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void enableDrawerLayout() {

        if( dlDrawer != null )
            dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onBackPressed() {

        try {

            if( dlDrawer.isDrawerOpen(GravityCompat.START) ) {
                dlDrawer.closeDrawer(GravityCompat.START);

                return;
            }

            switch (mCurrentFragmentIndex) {

                case FRAGMENT_SPLASH:
                    mSplashFragment.onBackPressed();
                    break;

                case FRAGMENT_MAIN:
                    mMainFragment.onBackPressed();
                    break;

                default:
                    super.onBackPressed();
                    break;
            }

        }catch (Exception e) {}
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////           GUI
    ////////////////////////////////////////////////////////////////////////////////////////////////
    private Fragment getFragment(FragmentID fragmentId) {
        switch (fragmentId) {
            case FRAGMENT_SPLASH:
                return mSplashFragment;

            case FRAGMENT_MAIN:
                return mMainFragment;

            case FRAGMENT_LOGIN:
                return mLoginFragment;

            default:
                TraLog.d(Constants.TAG.PARK, "[Main] get fragment error");
                break;
        }
        return null;
    }

    private void changeFragment(FragmentID fragmentId) {

        TraLog.d(Constants.TAG.PARK, "[Main] changeFragment : " + fragmentId.toString());

        if( fragmentId == FragmentID.FRAGMENT_MAIN ) {
            enableDrawerLayout();
        } else {
            disableDrawerLayout();
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(getFragment(fragmentId) == null)
            return;

        transaction.replace(R.id.layout_parent, getFragment(fragmentId));
        transaction.commitAllowingStateLoss();


        mPreviousFragmentIndex 	= mCurrentFragmentIndex;
        mCurrentFragmentIndex 	= fragmentId;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////           Service
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public void startTraService() {

        Intent intent = new Intent(this, TraMainService.class);
        startService(intent);
    }

    public void stopTraService() {

        Intent intent = new Intent(this, TraMainService.class);
        stopService(intent);
    }

    private void bindTraService() {

        Intent intent = new Intent(this, TraMainService.class);
        bindService(intent, mTraServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindTraService() {
        if(mServiceMessenger != null) {
            Message message = Message.obtain(null, Protocol.MSG_UNREGISTER_CLIENT);
            message.replyTo = mMainMessenger;
            sendToServiceMessenger(message);

            try {
                unbindService(mTraServiceConnection);
            }catch (Exception e) {}
        }
    }

    private boolean sendToServiceMessenger(Message message) {
        boolean result = false;
        if(mServiceMessenger == null) {
            return result;
        }

        try {
            mServiceMessenger.send(message);
            result = true;
        } catch (RemoteException e) {
            TraLog.d(Constants.TAG.PARK, String.format("[Main -> Service] Send to Service <error : %s>", e.toString()));
        }
        return result;
    }

    private class TraServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mServiceMessenger 	= new Messenger(service);
            Message message 	= Message.obtain(null, Protocol.MSG_REGISTER_CLIENT);
            message.replyTo 	= mMainMessenger;
            sendToServiceMessenger(message);

            if(mCurrentFragmentIndex == FragmentID.FRAGMENT_SPLASH) {
                mSplashFragment.onServiceConnected();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceMessenger = null;
        }
    }


    private static class MainMessageHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public MainMessageHandler(MainActivity service) {
            mActivity = new WeakReference<MainActivity>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity service = mActivity.get();
            if(service != null) {
                service.HandleMainMessage(msg);
            }
        }
    }

    private void HandleMainMessage(Message msg) {

        try {

            switch (msg.what) {

                case Protocol.MSG_TRA_WEB_CONNECTED:

                    if (mCurrentFragmentIndex == FragmentID.FRAGMENT_LOGIN) {

                        drawerAdapter.setHeaderAlias(Build.MODEL);
                        drawerAdapter.setHeaderUserGroup(traProperties.getMeshInfo().getName());
                        drawerAdapter.notifyDataSetChanged();

                        mLoginFragment.onLoginResult(true);
                    }
                    break;

                case Protocol.MSG_TRA_WEB_CONNECT_FAILURE:

                    if (mCurrentFragmentIndex == FragmentID.FRAGMENT_LOGIN) {
                        mLoginFragment.onLoginResult(false);
                    }
                    break;

                case Protocol.MSG_TRA_WEB_DISCONNECTED:

                    if (mCurrentFragmentIndex == FragmentID.FRAGMENT_LOGIN) {
                        mLoginFragment.onLoginResult(false);
                    }
                    break;

                /*
                case Protocol.MSG_TRA_CONNECT_SUCCESS:

                    if (mCurrentFragmentIndex == FragmentID.FRAGMENT_LOGIN) {
                        mLoginFragment.onLoginResult(true);
                    }
                    break;

                case Protocol.MSG_TRA_WEB_CONNECT_FAILURE:

                    if (mCurrentFragmentIndex == FragmentID.FRAGMENT_LOGIN) {
                        mLoginFragment.onLoginResult(false);
                    }
                    break;
                */
            }
        } catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "[MainActivity] HandleMainMessage exception : " + e.getMessage());
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////  Drawer Layout
    ////////////////////////////////////////////////////////////////////////////////////////////////
    private DrawerLayout dlDrawer;
    private ListView lvDrawerList;
    private ActionBarDrawerToggle dtToggle;
    private DrawerListAdapter drawerAdapter;
    private ArrayList<DrawerListItem> drawerData;

    private void initDrawerSetting() {

        lvDrawerList = (ListView) findViewById(R.id.lv_activity_main);
        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerData = new ArrayList<DrawerListItem>();

        //DrawerListItem eptItem = new DrawerListItem();
        //eptItem.Type = Constants.DRAWER.LIST_TYPE_EMPTY;

        // Header
        DrawerListItem headerItem = new DrawerListItem();
        headerItem.Type = Constants.DRAWER.LIST_TYPE_HEADER;
        drawerData.add(headerItem);

        // About
        DrawerListItem aboutItem = new DrawerListItem();
        aboutItem.Type = Constants.DRAWER.LIST_TYPE_ABOUT;
        aboutItem.setDefaultContent(getResources().getString(R.string.drawer_title_abount));
        drawerData.add(aboutItem);

        // Logout
        DrawerListItem logoutItem = new DrawerListItem();
        logoutItem.Type = Constants.DRAWER.LIST_TYPE_LOGOUT;
        logoutItem.setDefaultContent(getResources().getString(R.string.drawer_title_logout));
        drawerData.add(logoutItem);

        drawerAdapter = new DrawerListAdapter(this, drawerData);
        lvDrawerList.setAdapter(drawerAdapter);

        // Navigation drawer : ActionBar Toggle
        dtToggle = new ActionBarDrawerToggle(this, dlDrawer, R.string.app_name, R.string.app_name);
        dlDrawer.setDrawerListener(dtToggle);

        dlDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onClickHeaderClose() {
        closeDrawerLayout();
    }

    @Override
    public void onClickAbout() {
        closeDrawerLayout();
    }

    @Override
    public void onClickLogout() {
        runForceLogout();
    }

    private void runForceLogout() {
        Message message = Message.obtain(null, Protocol.MSG_FORCE_LOGOUT);
        sendToServiceMessenger(message);

        closeDrawerLayout();

        changeFragment(FragmentID.FRAGMENT_LOGIN);
    }
}
