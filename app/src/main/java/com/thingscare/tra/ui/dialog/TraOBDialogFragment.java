package com.thingscare.tra.ui.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingscare.tra.R;

public class TraOBDialogFragment extends DialogFragment {

    private View.OnClickListener buttonOkListener;
    private ImageView ok;
    private String m_comment;
    private String m_title;
    private TextView v_comment;
    private TextView v_title;

    public TraOBDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.tra_dialog_single_button, container);

        ok = (ImageView)view.findViewById(R.id.ok_onebutton);
        v_comment = (TextView) view.findViewById(R.id.comment);
        v_title = (TextView) view.findViewById(R.id.title);

        ok.setOnClickListener(buttonOkListener);

        if ( !m_comment.equals("") )
            v_comment.setText(m_comment);

        if ( !m_title.equals("") )
            v_title.setText(m_title);

        return view;
    }

    public void setTitleText(String title) {
        m_title = title;
    }

    public void setMessageText(String msg) {
        m_comment = msg;
    }

    public void setOkOnClickListener(View.OnClickListener listener) {
        buttonOkListener = listener;
    }

}
