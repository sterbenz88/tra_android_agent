package com.thingscare.tra.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thingscare.tra.R;
import com.thingscare.tra.ui.MainActivity;
import com.thingscare.tra.ui.dialog.TraOBDialogFragment;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;
import com.thingscare.tra.util.TraProperties;
import com.thingscare.tra.util.TraUtil;

import java.util.ArrayList;
import java.util.List;

public class SplashFragment extends Fragment {

    private volatile ServiceConnected mServiceConnected;

    private SplashFragmentListener 	mMainActivity;
    private Context mContext;

    private TraProperties traProperties;

    private TraOBDialogFragment mLmmOBDialogFragment;
    private boolean mNetworkConnected;

    private AlertDialog.Builder whiteListDialog = null;

    private static final int REQUEST_PERMISSIONS = 1;
    private static final int REQUEST_OVERLAY_PERMISSION = 2;
    private static final int REQUEST_BATTERY_PERMISSION = 3;
    private static final String[] REQUEST_PERMISSIONS_LIST = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public SplashFragment() {

        traProperties = TraProperties.getInstance();
        mServiceConnected			= new ServiceConnected();
    }

    private class ServiceConnected {
        public boolean connected = false;
    }

    public interface SplashFragmentListener {
        void onReqChangeFragment(MainActivity.FragmentID fragmentId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_splash, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mMainActivity = (SplashFragmentListener)getActivity();
        mContext = getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        CheckAsyncTask checkAsyncTask = new CheckAsyncTask();
        checkAsyncTask.execute();
    }

    public void onBackPressed() {
        // Do nothing.
    }

    public void onServiceConnected() {
        synchronized(mServiceConnected){
            mServiceConnected.connected = true;
            mServiceConnected.notifyAll();
        }
    }

    public class CheckAsyncTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            if(!TraUtil.isNetWorkAvailable(mContext)) {
                mNetworkConnected = false;
            } else {
                mNetworkConnected = true;
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                Thread.sleep(1500);
            }catch (Exception e) {}

            boolean result = true;

            synchronized(mServiceConnected){
                if(!mServiceConnected.connected) {
                    try {
                        mServiceConnected.wait();
                    } catch (InterruptedException e) {
                        Log.v(Constants.TAG.PARK, String.format("[Main] Service Connect Wait <error : %s>", e.toString()));
                    }
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if( !result )
                return;

            if(!mNetworkConnected) {
                showNetworkUnavailableDialog();
                return;
            }
            checkPermission();
        }
    }

    private void checkPermission() {

        if(checkAndRequestPermission(getActivity(), REQUEST_PERMISSIONS, REQUEST_PERMISSIONS_LIST)) {
            if(checkAndRequestSystemOverlay(getActivity(), REQUEST_OVERLAY_PERMISSION)) {

                if(!getWhiteList()) {
                    runWhiteListProcess();
                }
                else {
                    checkCompleteAndMoveNextPage();
                }
            }
        }
    }

    private void checkCompleteAndMoveNextPage() {
        traProperties.setPassSplash(true);
        mMainActivity.onReqChangeFragment(MainActivity.FragmentID.FRAGMENT_LOGIN);
    }

    private boolean getWhiteList() {

        boolean isWhiteListing = true;
        try {

            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                isWhiteListing = pm.isIgnoringBatteryOptimizations(mContext.getPackageName());
            }

        }catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "SplashFragment getWhiteList exception : " + e.getMessage());
        }

        return isWhiteListing;
    }

    private void runWhiteListProcess() {

        if( whiteListDialog != null)
            return;

        whiteListDialog = new AlertDialog.Builder(mContext);
        whiteListDialog.setTitle(mContext.getResources().getString(R.string.permission_request_title))
                .setMessage(mContext.getResources().getString(R.string.permission_whitelist_warning_text))
                .setPositiveButton(mContext.getResources().getString(R.string.permission_request_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
                            startActivityForResult(intent, REQUEST_BATTERY_PERMISSION);
                        }
                    }
                })
                .setNegativeButton(mContext.getResources().getString(R.string.permission_request_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), mContext.getResources().getString(R.string.permission_request_cancel), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                })
                .create()
                .show();
    }

    private void showNetworkUnavailableDialog() {

        if( mLmmOBDialogFragment != null ) {
            mLmmOBDialogFragment.dismiss();
            mLmmOBDialogFragment = null;
        }

        mLmmOBDialogFragment = new TraOBDialogFragment();
        mLmmOBDialogFragment.setCancelable(false);
        mLmmOBDialogFragment.setTitleText(getResources().getString(R.string.dialog_title_default));
        mLmmOBDialogFragment.setMessageText(getResources().getString(R.string.dialog_network_unavailable));
        mLmmOBDialogFragment.setOkOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mLmmOBDialogFragment.dismiss();
                getActivity().finish();
            }
        });
        mLmmOBDialogFragment.show(getFragmentManager(), "");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        TraLog.d(Constants.TAG.PARK, "[SplashFragment] onActivityResult " + requestCode + ", " + resultCode);

        if (requestCode == REQUEST_OVERLAY_PERMISSION) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (Settings.canDrawOverlays(getActivity())) {
                    if (checkAndRequestPermission(getActivity(), REQUEST_PERMISSIONS, REQUEST_PERMISSIONS_LIST)) {

                        if (!getWhiteList()) {
                            runWhiteListProcess();
                        } else {
                            checkCompleteAndMoveNextPage();
                        }
                    }

                } else {
                    showAlertDialogPermissionDenied();
                }
            }
            else {

                if (checkAndRequestPermission(getActivity(), REQUEST_PERMISSIONS, REQUEST_PERMISSIONS_LIST)) {

                    if (!getWhiteList()) {
                        runWhiteListProcess();
                    } else {
                        checkCompleteAndMoveNextPage();
                    }
                }
            }
        }
        else if (requestCode == REQUEST_BATTERY_PERMISSION) {

            if (!getWhiteList()) {

                showAlertDialogPermissionDenied();
            } else {
                checkCompleteAndMoveNextPage();
            }
        }
    }

    public static String[] getRequiredPermissions(Context context, String... permissions) {
        List<String> requiredPermissions = new ArrayList<String>();

        if (context == null)
            return requiredPermissions.toArray(new String[1]);

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                requiredPermissions.add(permission);
            }
        }

        return requiredPermissions.toArray(new String[requiredPermissions.size()]);
    }

    public boolean checkAndRequestPermission(Activity activity, int permissionRequestCode,
                                             String... permissions) {
        String[] requiredPermissions = getRequiredPermissions(activity, permissions);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return true;
        }

        if (requiredPermissions.length > 0 && activity != null && !activity.isDestroyed()) {
            requestPermissions(requiredPermissions, permissionRequestCode);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkAndRequestSystemOverlay(Activity activity, int permissionRequestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        try {
            if (!Settings.canDrawOverlays(getActivity())) {

                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getContext().getPackageName()));
                startActivityForResult(intent, REQUEST_OVERLAY_PERMISSION);
                return false;
            }
        }catch (Exception e) {}

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        TraLog.d(Constants.TAG.PARK, "[SplashFragment] onRequestPermissionsResult " + requestCode);

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS) {
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    showAlertDialogPermissionDenied();
                    return;
                }
            }

            if(checkAndRequestSystemOverlay(getActivity(), REQUEST_OVERLAY_PERMISSION)) {

                if(!getWhiteList()) {
                    runWhiteListProcess();
                }
                else {
                    checkCompleteAndMoveNextPage();
                }
            }
        }
    }

    public void showAlertDialogPermissionDenied() {

        Toast.makeText(mContext, mContext.getResources().getString(R.string.permission_request_cancel), Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }
}
