package com.thingscare.tra.protocol;

public class Protocol {

    public final static short MSG_REGISTER_CLIENT                   = 1;
    public final static short MSG_UNREGISTER_CLIENT                 = 2;
    public final static short MSG_TRA_WEB_CONNECT                   = 3;
    public final static short MSG_TRA_WEB_CONNECT_CANCEL            = 4;
    public final static short MSG_TRA_WEB_CONNECT_FAILURE           = 5;
    public final static short MSG_TRA_WEB_CONNECTED                 = 6;
    public final static short MSG_TRA_WEB_DISCONNECTED              = 7;

    public final static short MSG_FORCE_LOGOUT                      = 8;

    public final static short SERVER_MESSAGE_ERROR                  = 0;
    public final static short SERVER_MESH_INFO                      = 1000;
    public final static short SERVER_REGISTER_INFO                  = 1001;
}
