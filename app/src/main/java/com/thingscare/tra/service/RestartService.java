package com.thingscare.tra.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;

public class RestartService extends BroadcastReceiver {


    public static final String ACTION_RESTART_BACKGROUNDSERVICE
            = "ACTION.Restart.TraBackgroundService";

    @Override
    public void onReceive(Context context, Intent intent) {

        TraLog.d(Constants.TAG.PARK, "RestartService called.");

        /* 서비스 죽일때 알람으로 다시 서비스 등록 */
        if (intent.getAction().equals(ACTION_RESTART_BACKGROUNDSERVICE)) {

            TraLog.d(Constants.TAG.PARK, "Service dead, but resurrection");

            Intent mIntent = new Intent(context, TraMainService.class);
            context.startService(mIntent);
        }

        /* 폰 재부팅할때 서비스 등록 */
//        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
//
//            Log.d("RestartService", "ACTION_BOOT_COMPLETED");
//
//            Intent i= new Intent(context, MainActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//            context.startActivity(i);
//        }
    }
}
