package com.thingscare.tra.service;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thingscare.tra.common.TraNotificationManager;
import com.thingscare.tra.protocol.Protocol;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraLog;
import com.thingscare.tra.util.TraPreference;
import com.thingscare.tra.util.TraProperties;
import com.thingscare.tra.web.WebSession;
import com.thingscare.tra.web.WebSessionListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class TraMainService extends Service implements WebSessionListener {

    private Context mContext;

    private TraPreference traPreference;
    private TraProperties traProperties;

    private ServiceMessageHandler mServiceMessageHandler;
    private Messenger mServiceMessenger;
    private ArrayList<Messenger> mClients;

    private WebSession webSession;

    private static PowerManager.WakeLock sCpuWakeLock;
    private DozeStateReceiver				mDozeStateReceiver;
    private boolean isDoze = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mServiceMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TraLog.d("Park", "TraMainService onCreate.");

        unregisterRestartAlarm();
        initInstance();

        if (!traProperties.getIsActivityRun())
            autoRunStart();
    }

    @Override
    public void onDestroy() {
        destroyInstance();

        boolean sharedValueRememberAccount = traPreference.getValue(TraPreference.PREF_AUTO_LOGIN, false);
        if (sharedValueRememberAccount)
            registerRestartAlarm();

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        initAutoRun(intent);

        return super.onStartCommand(intent, flags, startId);
    }

    private void initAutoRun(Intent mIntent) {

        try {
            if (mIntent == null || mIntent.getExtras() == null) {
                return;
            }

            boolean isAutoRun = mIntent.getExtras().getBoolean(Constants.DEFAULT.FLAG_AUTO_RUN, false);
            String savedNodeID = traPreference.getValue(TraPreference.PREF_NODE_ID, "");
            if( savedNodeID.equals("") ) {
                TraLog.e(Constants.TAG.PARK, "[TraMainService] initAutoRun terminate. [NO NODE ID]");
                return;
            }

            if (isAutoRun) {
                autoRunStart();
            }
        }catch (Exception e) {}
    }

    public void autoRunStart() {

        try {
            boolean sharedValueRememberAccount = traPreference.getValue(TraPreference.PREF_AUTO_LOGIN, false);
            String savedNodeID = traPreference.getValue(TraPreference.PREF_NODE_ID, "");

            if (sharedValueRememberAccount) {

                TraLog.d(Constants.TAG.PARK, "[TraMainService] autoRunStart start.");

                //String savedNodeID = traPreference.getValue(TraPreference.PREF_NODE_ID, "");

                //traProperties.setBeAutoRunRestRequestStatue(true);

                //String modelName = "Android";
                //try {
                //    modelName = Build.MODEL;
                //}catch (Exception e) {}

                traProperties.setIsAutoRun(true);
                traProperties.setNodeid(savedNodeID);

                //RunWakeUp();

                // Login.
                String serverAddress = traPreference.getValue(TraPreference.PREF_SERVER_ADDRESS, Constants.DEFAULT.SERVER_ADDRESS);
                webSession.runWebSocket(serverAddress);

                Log.e("Park", "[TraMainService] autoRunStart end.");
            }

        }catch (Exception e) {}
    }

    private void initInstance() {

        mContext = this;

        mClients = new ArrayList<Messenger>();
        mServiceMessageHandler = new ServiceMessageHandler(this);
        mServiceMessenger	 = new Messenger(mServiceMessageHandler);

        webSession = new WebSession(mContext);
        webSession.setListener(this);

        traPreference = new TraPreference(this);
        traProperties = TraProperties.getInstance();

        mServiceMessageHandler.post(new Runnable() {
            @Override
            public void run() {
                TraNotificationManager.showConnectionNotification(TraMainService.this);
            }
        });

        registerReceivers();
    }

    private void destroyInstance() {

        try {
            TraLog.e("Park", "TraMainService destroyInstance.");

            if (webSession != null)
                webSession.retryCancel();

            mServiceMessageHandler.post(new Runnable() {
                @Override
                public void run() {
                    TraNotificationManager.dismissConnectionNotification(TraMainService.this);
                }
            });

        }catch (Exception e) {
            // TODO
        }finally {
            unRegisterReceivers();
        }
    }

    @Override
    public void updateWebSession(short Command, Message msg) {

        switch (Command) {
            case Protocol.MSG_TRA_WEB_CONNECTED:

                traPreference.put(TraPreference.PREF_NODE_ID, traProperties.getRegisterInfo().getNodeid());
                traPreference.put(TraPreference.PREF_AUTO_LOGIN, true);

                traProperties.setIsAutoRun(true);
                traProperties.setNodeid(traProperties.getRegisterInfo().getNodeid());

                traProperties.setServerConn(true);

                Message myMsg = Message.obtain(null, Protocol.MSG_TRA_WEB_CONNECTED);
                sendMessageToClients(myMsg);
                break;

            case Protocol.MSG_TRA_WEB_CONNECT_FAILURE:

                traProperties.setServerConn(false);

                myMsg = Message.obtain(null, Protocol.MSG_TRA_WEB_CONNECT_FAILURE);
                sendMessageToClients(myMsg);
                break;

            case Protocol.MSG_TRA_WEB_DISCONNECTED:

                traProperties.setServerConn(false);

                myMsg = Message.obtain(null, Protocol.MSG_TRA_WEB_DISCONNECTED);
                sendMessageToClients(myMsg);
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////           Main interface
    ////////////////////////////////////////////////////////////////////////////////////////////////
    private static class ServiceMessageHandler extends Handler {
        private final WeakReference<TraMainService> mService;

        public ServiceMessageHandler(TraMainService service) {
            mService = new WeakReference<TraMainService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            TraMainService service = mService.get();
            if(service != null) {
                service.HandleServiceMessage(msg);
            }
        }
    }

    private void HandleServiceMessage(Message msg) {

        Bundle mBundle;

        if( msg == null )
            return;

        switch (msg.what) {
            case Protocol.MSG_REGISTER_CLIENT:
                TraLog.d(Constants.TAG.PARK, String.format("[Main] register client!"));
                mClients.clear();
                mClients.add(msg.replyTo);
                break;

            case Protocol.MSG_UNREGISTER_CLIENT:
                TraLog.d(Constants.TAG.PARK, String.format("[Main] unregister client!"));
                mClients.remove(msg.replyTo);
                break;

            case Protocol.MSG_TRA_WEB_CONNECT:
                TraLog.d(Constants.TAG.PARK, String.format("[Main] MSG_TRA_WEB_CONNECT!"));

                mBundle = msg.getData();
                String serverAddress = mBundle.getString("server");

                webSession.runWebSocket(serverAddress);
                break;

            case Protocol.MSG_TRA_WEB_CONNECT_CANCEL:

                webSession.retryCancel();
                break;

            case Protocol.MSG_FORCE_LOGOUT:

                traPreference.put(TraPreference.PREF_NODE_ID, "");
                traPreference.put(TraPreference.PREF_AUTO_LOGIN, false);

                traProperties.setIsAutoRun(false);
                traProperties.setNodeid("");

                traProperties.setServerConn(false);

                webSession.retryCancel();
                webSession.close();
                break;
        }
    }

    private int sendMessageToClients(Message msg) {
        for (int i = 0; i < mClients.size(); i++) {
            try {
                mClients.get(i).send(msg);
            } catch (RemoteException e) {
                mClients.remove(i);
            }
        }

        return mClients.size();
    }

    private void registerReceivers() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mDozeStateReceiver = new DozeStateReceiver();
            IntentFilter intentFilter = new IntentFilter(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
            registerReceiver(mDozeStateReceiver, intentFilter);
        }
    }

    private void unRegisterReceivers() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if(mDozeStateReceiver != null) {
                unregisterReceiver(mDozeStateReceiver);
            }
        }
    }

    private void registerRestartAlarm(){

        //Log.i("000 PersistentService" , "registerRestartAlarm" );
        Intent intent = new Intent(TraMainService. this,RestartService.class);
        intent.setAction(RestartService.ACTION_RESTART_BACKGROUNDSERVICE);
        PendingIntent sender = PendingIntent.getBroadcast(TraMainService.this,0,intent,0);

        long firstTime = SystemClock.elapsedRealtime();
        firstTime += 2 * 1000;

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,2 * 1000, sender);
    }

    private void unregisterRestartAlarm(){

        Intent intent = new Intent(TraMainService.this,RestartService.class);
        intent.setAction(RestartService.ACTION_RESTART_BACKGROUNDSERVICE);
        PendingIntent sender = PendingIntent.getBroadcast(TraMainService.this,0,intent,0);

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

        alarmManager.cancel(sender);
    }

    @TargetApi(23)
    private class DozeStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                PowerManager powerManager = context.getSystemService(PowerManager.class);
                isDoze = powerManager.isDeviceIdleMode();

                //if (isDoze) {
                //    checkDeviceIdleWhitelist();
                //}
            }catch (Exception e) {}
        }
    }

    private void RunWakeUp() {

        try {
            if (sCpuWakeLock != null) {
                return;
            }
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            sCpuWakeLock = pm.newWakeLock(
                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                            PowerManager.ACQUIRE_CAUSES_WAKEUP |
                            PowerManager.ON_AFTER_RELEASE, "lmm:hi");

            sCpuWakeLock.acquire();

            if (sCpuWakeLock != null) {
                sCpuWakeLock.release();
                sCpuWakeLock = null;
            }

        }catch (Exception e) {}
    }
}


