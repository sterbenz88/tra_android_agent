package com.thingscare.tra.common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.thingscare.tra.R;
import com.thingscare.tra.ui.MainActivity;

public class TraNotificationManager {

    private static final String  				NOTIFICATION_CHANNEL_ID  				= "tra_channel_main";
    public static int                           NOTIFICATION_ID_CONNECTION 		        = 38389;

    public TraNotificationManager() {

    }

    public static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.icon_wh : R.mipmap.ic_launcher;
    }

    public static void showConnectionNotification(Service context) {

        Intent intent 			= new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("fragmentID", MainActivity.FragmentID.FRAGMENT_MAIN.ordinal());

        PendingIntent pendingIntent 	= PendingIntent.getActivity(context, 0, intent, 0);

        Notification.Builder builder;
        if(Build.VERSION.SDK_INT >= 26) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "LinkMeMine", NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);

            builder = new Notification.Builder(context.getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        } else {
            builder = new Notification.Builder(context);
        }

        int color = context.getResources().getColor(R.color.blue2);

        Notification notification;
        if(Build.VERSION.SDK_INT >= 23) {
            notification = builder.setSmallIcon(getNotificationIcon())
                    .setColor(color)
                    .setContentTitle(context.getResources().getString(R.string.notify_title))
                    .setContentText(context.getResources().getString(R.string.notify_text))
                    .setTicker(context.getResources().getString(R.string.notify_ticker))
                    .setContentIntent(pendingIntent).setAutoCancel(false).build();
        } else {
            notification = builder.setSmallIcon(getNotificationIcon())
                    .setContentTitle(context.getResources().getString(R.string.notify_title))
                    .setContentText(context.getResources().getString(R.string.notify_text))
                    .setTicker(context.getResources().getString(R.string.notify_ticker))
                    .setContentIntent(pendingIntent).setAutoCancel(false).build();
        }

        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        context.startForeground(NOTIFICATION_ID_CONNECTION, notification);
    }

    public static void dismissConnectionNotification(Service context) {

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID_CONNECTION);
        context.stopForeground(true);
    }

    public static void startHiddenForground(Service context, int startId)
    {
        /**
         *  startForeground 를 사용하면 notification 을 보여주어야 하는데 없애기 위한 코드
         */
        context.startForeground(startId, new Notification());
        NotificationManager nm = (NotificationManager)context.getSystemService(Service.NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(context.getApplicationContext())
                .setContentTitle("")
                .setContentText("")
                .build();

        nm.notify(startId, notification);
        nm.cancel(startId);
    }

}
