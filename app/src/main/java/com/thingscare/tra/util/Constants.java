package com.thingscare.tra.util;

public class Constants {

    public interface TAG {
        public static String PARK = "Park";
    }

    public interface DEFAULT {
        public static String SERVER_ADDRESS = "wss://tra.thingscare.com:8080/android.ashx";
        public static String FLAG_AUTO_RUN = "FLAG_AUTO_RUN";
    }

    public interface DRAWER {
        public static int LIST_TYPE_HEADER = 0;
        public static int LIST_TYPE_EMPTY = 1;

        public static int LIST_TYPE_ABOUT = 2;

        public static int LIST_TYPE_LOGOUT = 99;
    }

    public interface NOTIFICATION_ID {
        public static int NOTIFICATION_ID_CONNECTION = 18289;
    }
}
