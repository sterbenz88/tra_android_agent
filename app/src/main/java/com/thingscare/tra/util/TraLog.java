package com.thingscare.tra.util;

import android.util.Log;

public class TraLog {
    public static boolean DEBUG = true;

    public static void setDebugMode(boolean enable) {
        DEBUG = enable;
    }

    public static void i(String tag, String message) {
        if(DEBUG) Log.i(tag, message);
    }

    public static void w(String tag, String message) {
        if(DEBUG) Log.w(tag, message);
    }

    public static void d(String tag, String message) {
        if(DEBUG) Log.d(tag, message);
    }

    public static void e(String tag, String message) {
        if(DEBUG) Log.e(tag, message);
    }

    public static void v(String tag, String message) {
        if(DEBUG) Log.v(tag, message);
    }
}
