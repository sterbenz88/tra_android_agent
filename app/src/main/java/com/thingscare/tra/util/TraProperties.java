package com.thingscare.tra.util;

import com.thingscare.tra.data.MeshInfo;
import com.thingscare.tra.data.RegisterInfo;

public class TraProperties {

    private volatile static TraProperties uniqueInstance;

    private MeshInfo meshInfo;
    private RegisterInfo registerInfo;

    private String nodeid;

    private boolean serverConn = false;
    private boolean passSplash;
    private boolean isActivityRun = false;
    private boolean isAutoRun;

    public static TraProperties getInstance() {
        if (uniqueInstance == null) {
            synchronized (TraProperties.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new TraProperties();
                }
            }
        }
        return uniqueInstance;
    }

    public TraProperties() {
        passSplash = false;
    }

    public void setPassSplash(boolean passSplash) {
        this.passSplash = passSplash;
    }
    public boolean getPassSplash() { return this.passSplash; }

    public void setIsActivityRun(boolean activityRun) {
        isActivityRun = activityRun;
    }

    public boolean getIsActivityRun() { return this.isActivityRun; }

    public void setIsAutoRun(boolean isAutoRun) {
        this.isAutoRun = isAutoRun;
    }

    public boolean getIsAutoRun() { return isAutoRun; }

    public void setMeshInfo(MeshInfo meshInfo) {
        this.meshInfo = meshInfo;
    }

    public MeshInfo getMeshInfo() {
        return meshInfo;
    }

    public void setRegisterInfo(RegisterInfo registerInfo) {
        this.registerInfo = registerInfo;
    }

    public RegisterInfo getRegisterInfo() {
        return registerInfo;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }

    public String getNodeid() {
        return nodeid;
    }

    public void setServerConn(boolean serverConn) {
        this.serverConn = serverConn;
    }

    public boolean getServerConn() {
        return this.serverConn;
    }
}
