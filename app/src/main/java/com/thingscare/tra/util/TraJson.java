package com.thingscare.tra.util;

import com.thingscare.tra.data.MeshInfo;
import com.thingscare.tra.data.RegisterInfo;
import com.thingscare.tra.protocol.Protocol;

import org.json.JSONArray;
import org.json.JSONObject;

public class TraJson {

    public static final String MAIN_ACTION = "action";
    public static final String REQUEST_MESHES = "meshes";
    public static final String REQUEST_REGISTER = "register";
    public static final String REQUEST_COREINFO = "coreinfo";

    public static final String RESPONSE_MESHES = "meshes";
    public static final String RESPONSE_REGISTERED = "registered";

    public static short parsingMessageType(String jsonString) {

        try {
            JSONObject jsonParams = new JSONObject(jsonString);

            String actionString = jsonParams.getString(MAIN_ACTION);
            switch (actionString) {
                case RESPONSE_MESHES:
                    return Protocol.SERVER_MESH_INFO;

                case RESPONSE_REGISTERED:
                    return Protocol.SERVER_REGISTER_INFO;

                default:
                    return Protocol.SERVER_MESSAGE_ERROR;
            }

        }catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "TraJson parsingMessage : " + e.getMessage());
        }
        return 0;
    }

    public static MeshInfo parsingMeshInfo(String jsonString) {
        MeshInfo meshInfo = null;

        try {
            JSONObject jsonParams = new JSONObject(jsonString);
            JSONArray jsonArray = jsonParams.getJSONArray("data");

            // TODO. 우선 첫번째 데이터를 가져온다. 추후 수정
            for (int i=0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                int consent = 0;
                try {
                    consent = jsonObject.getInt("consent");
                }catch (Exception e) {}

                int flags = 0;
                try {
                    flags = jsonObject.getInt("flags");
                }catch (Exception e) {}

                meshInfo = new MeshInfo(jsonObject.getString("_id"),
                                        jsonObject.getString("meshid"),
                                        jsonObject.getString("name"),
                                        jsonObject.getString("mtype"),
                                        jsonObject.getString("desc"),
                                        jsonObject.getString("domain"),
                                        jsonObject.getJSONObject("links"),
                                        consent,
                                        flags);
                break;
            }

        }catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "TraJson parsingMeshInfo : " + e.getMessage());
        }

        return meshInfo;
    }

    public static RegisterInfo parsingRegisterInfo(String jsonString) {
        RegisterInfo registerInfo = null;

        try {
            JSONObject jsonParams = new JSONObject(jsonString);
            String nodeid = jsonParams.getString("nodeid");

            registerInfo = new RegisterInfo(nodeid);

        }catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, "TraJson parsingRegisterInfo : " + e.getMessage());
        }

        return registerInfo;
    }

    public static JSONObject generateActionMashes() {
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put(MAIN_ACTION, REQUEST_MESHES);

            return jsonParams;
        }catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public static JSONObject generateActionRegister(String meshid,
                                                    int infoVersion,
                                                    int agentId,
                                                    int agentVersion,
                                                    int platformType,
                                                    int capabilities,
                                                    String computerName,
                                                    String nodeid) {
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put(MAIN_ACTION, REQUEST_REGISTER);

            jsonParams.put("meshid", meshid);
            jsonParams.put("infoVersion", infoVersion);
            jsonParams.put("agentId", agentId);
            jsonParams.put("agentVersion", agentVersion);
            jsonParams.put("platformType", platformType);
            jsonParams.put("capabilities", capabilities);
            jsonParams.put("computerName", computerName);

            if (nodeid != null && !nodeid.isEmpty()) {
                jsonParams.put("nodeid", nodeid);
            }

            return jsonParams;
        }catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public static JSONObject generateActionCoreinfo(String value,
                                                    int caps) {
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put(MAIN_ACTION, REQUEST_COREINFO);

            jsonParams.put("value", value);
            jsonParams.put("caps", caps);
            jsonParams.put("osdesc", "Android");

            JSONArray jsonAvArray = new JSONArray();
            JSONObject jsonAvParams = new JSONObject();
            jsonAvParams.put("product", "Windows Defender");
            jsonAvParams.put("updated", true);
            jsonAvParams.put("enabled", true);
            jsonAvArray.put(jsonAvParams);

            jsonParams.put("av", jsonAvArray);

            return jsonParams;
        }catch (Exception e) {
            throw new NullPointerException();
        }
    }
}
