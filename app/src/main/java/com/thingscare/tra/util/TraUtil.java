package com.thingscare.tra.util;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class TraUtil {
    public static String percentage(Long all, Long used){
        return String.valueOf((100*used/all));
    }
    public static String percentage2(Long all, Long used){
        return String.valueOf(Math.round((double)((double)((double)all-(double)used)/all)*100));
    }
    public static boolean isNull(String str){
        if (str == null || "".equals(str) || str.trim().length() == 0){
            return true;
        }else{
            return false;
        }
    }
    public static boolean isNotNull(String str){
        return !isNull(str);
    }
    public static  String strDef(String str,String der){
        if (str == null || "".equals(str) || str.trim().length() == 0){
            return der.trim();
        }else{
            return str.trim();
        }
    }
    public static String arrayToString(String[] arr){
        StringBuilder sb = new StringBuilder();
        for (String n : arr) {
            if (sb.length() > 0) sb.append(',');
            sb.append("'").append(n).append("'");
        }
        return sb.toString();
    }

    public static int getDeviceDefaultOrientation(Context context) {
        WindowManager windowManager =  (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Configuration config = context.getResources().getConfiguration();
        int rotation = windowManager.getDefaultDisplay().getRotation();

        if ( ((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) &&
                config.orientation == Configuration.ORIENTATION_LANDSCAPE)
                || ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) &&
                config.orientation == Configuration.ORIENTATION_PORTRAIT)) {
            return Configuration.ORIENTATION_LANDSCAPE;
        } else {
            return Configuration.ORIENTATION_PORTRAIT;
        }
    }

    public static boolean isTablet(Context context) {
        int xlargeBit = 4; // Configuration.SCREENLAYOUT_SIZE_XLARGE;  // upgrade to HC SDK to get this
        Configuration config = context.getResources().getConfiguration();
        return (config.screenLayout & xlargeBit) == xlargeBit;
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isEmail(String email) {

        if( email == null ) return false;
//
        if (email.isEmpty()) return false;
//        boolean b = Pattern.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+", email.trim());
//        return b;
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public static boolean isAccount(String account) {
        if(account == null) return false;
        if(account.isEmpty()) return false;
        if(account.length() > 48) return false;

        return true;
    }

    public static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{8,12}$");

    public static boolean isPasswordValidation(String pw) {

        if(pw == null) return false;

        if(pw.isEmpty()) return  false;
//        boolean b = Pattern.matches("^[a-zA-Z0-9!@.#$%^&*?_~]{8,12}$", pw.trim());
//        return b;
        Matcher matcher = VALID_PASSWOLD_REGEX_ALPHA_NUM.matcher(pw);
        return matcher.matches();
    }

    public static int getDensityName(Context context) {

        try {

            float density = context.getResources().getDisplayMetrics().density;
            if (density >= 4.0) {
                return 144;
            }
            if (density >= 3.0) {
                return 144;
            }
            if (density >= 2.0) {
                return 96;
            }
            if (density >= 1.5) {
                return 72;
            }
            if (density >= 1.0) {
                return 48;
            }

        }catch (Exception getDensityNameException) {
            TraLog.e(Constants.TAG.PARK, "getDensityNameException : " + getDensityNameException);
        }

        return 48;
    }

    public static String intToIp(int i) {

        return ( i & 0xFF) + "." +
                ((i >> 8 ) & 0xFF) + "." +
                ((i >> 16 ) & 0xFF) + "." +
                ((i >> 24 ) & 0xFF ) ;
    }


    public static String getPhoneName(Context mContext)
    {
        PackageManager pm = mContext.getPackageManager();
        boolean hasBluetooth = pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH);

        if( hasBluetooth ) {
            BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
            String deviceName = myDevice.getName();
            return deviceName;
        } else {
            return null;
        }
    }

    public static int getApplicationVersion(Context context, String packageName) {
        int applicationVersion = -1;
        PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo 			= pm.getPackageInfo(packageName, 0);
            if(packageInfo != null) {
                applicationVersion 	= packageInfo.versionCode;
            }
        } catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, String.format("[Main] check application version <error : %s>", e.toString()));
        }

        return applicationVersion;
    }

    public static String getApplicationVersionName(Context context, String packageName) {
        String mAddonAppVersionName = null;
        PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo 			= pm.getPackageInfo(packageName, 0);
            if(packageInfo != null) {
                mAddonAppVersionName 	= packageInfo.versionName;
            }
        } catch (Exception e) {
            TraLog.e(Constants.TAG.PARK, String.format("[Main] check application version <error : %s>", e.toString()));
        }

        return mAddonAppVersionName;
    }

    public static void installAppFromMarket(Context context, String packageName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("market://details?id=" + packageName));
        context.startActivity(intent);
    }

    public static boolean isNetWorkAvailable(Context context){
        boolean isMobileAvailable 	= false;
        boolean isMobileConnect		= false;
        boolean isWifiAvailable 	= false;
        boolean isWifiConnect		= false;

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService (Context.CONNECTIVITY_SERVICE);
        if(manager == null) {
            return false;
        }

        NetworkInfo mobile 	= manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi 	= manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if(mobile != null) {
            isMobileAvailable 	= mobile.isAvailable();
            isMobileConnect 	= mobile.isConnectedOrConnecting();
        }

        if(wifi != null) {
            isWifiAvailable 	= wifi.isAvailable();
            isWifiConnect 		= wifi.isConnectedOrConnecting();
        }

        if ((isWifiAvailable && isWifiConnect) || (isMobileAvailable && isMobileConnect)){
            return true;
        }else{
            return false;
        }
    }

    public static HashMap<String, String> xml2HashMap(String tmpData, String encoding, String tag) {
        HashMap<String, String> resultHashMap 				= new HashMap<String, String>();
        resultHashMap.put("count", "0");
        try {
            DocumentBuilderFactory documentBuilderFactory 	= DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder 				= documentBuilderFactory.newDocumentBuilder();
            InputStream inputStream 						= new ByteArrayInputStream(tmpData.getBytes(encoding));
            Document document 								= documentBuilder.parse(inputStream);
            Element element 								= document.getDocumentElement();
            NodeList dataList 								= element.getElementsByTagName(tag);
            NodeList dataNodeList 							= dataList.item(0).getChildNodes();

            for (int i = 0; i < dataNodeList.getLength(); i++) {
                Node itemNode = dataNodeList.item(i);
                if (itemNode.getFirstChild() != null) {
                    String nodeName 	= itemNode.getNodeName();
                    String nodeValue 	= itemNode.getFirstChild().getNodeValue();
                    resultHashMap.put(nodeName, nodeValue);
                    Log.v(Constants.TAG.PARK, String.format("[Main] xml2String <nodeName : %s><nodeValue : %s>", nodeName, nodeValue));
                }
            }
        } catch (Exception e) {
            Log.v(Constants.TAG.PARK, String.format("[Main] xml2String <error : %s>", e.toString()));
        }
        return resultHashMap;
    }

    public static void writeLog(String str) {
        String str_Path_Full = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lmm_log.txt";

        File file = new File(str_Path_Full);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {

            }
        } else {
            try {
                BufferedWriter bfw = new BufferedWriter(new FileWriter(str_Path_Full,true));
                bfw.write(str+"\r\n");
                //bfw.write("\r\n");
                bfw.flush();
                bfw.close();
            } catch (FileNotFoundException e) {
                TraLog.d(Constants.TAG.PARK, "LOG File FileNotFoundException - " + e );
            } catch (IOException e) {
                TraLog.d(Constants.TAG.PARK, "LOG File IOException - " + e );
            }
        }
    }

    /** 외장메모리 sdcard 사용가능한지에 대한 여부 판단 */
    public static boolean isStorage(boolean requireWriteAccess) {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        } else if (!requireWriteAccess &&
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    String sAddr = addr.getHostAddress().toUpperCase();

                    if(!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                        return sAddr;
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }
}
