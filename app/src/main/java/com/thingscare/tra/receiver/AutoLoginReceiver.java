package com.thingscare.tra.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.thingscare.tra.service.TraMainService;
import com.thingscare.tra.util.Constants;
import com.thingscare.tra.util.TraPreference;
import com.thingscare.tra.util.TraProperties;

public class AutoLoginReceiver extends BroadcastReceiver {

    private TraPreference traPreference;
    private TraProperties traProperties;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action= intent.getAction();

        if( action.equals("android.intent.action.BOOT_COMPLETED") ){

            traProperties = TraProperties.getInstance();
            traPreference = new TraPreference(context);
            boolean isAutoRunning = traPreference.getValue(TraPreference.PREF_AUTO_LOGIN, false);

            if ( traProperties.getIsActivityRun() ) {
                Log.e(Constants.TAG.PARK, "BOOT_COMPLETED already activity active.");
                return;
            }

            if( isAutoRunning ) {

                Intent mIntent = new Intent(context, TraMainService.class);
                mIntent.putExtra(Constants.DEFAULT.FLAG_AUTO_RUN, true);

                context.startService(mIntent);
            }
        }
    }
}
