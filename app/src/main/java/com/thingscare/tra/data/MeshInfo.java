package com.thingscare.tra.data;

import org.json.JSONObject;

public class MeshInfo {

    private String _id;
    private String meshid;
    private String name;
    private String mtype;
    private String desc;
    private String domain;
    private JSONObject links;
    private int consent;
    private int flags;

    public MeshInfo() {}

    public MeshInfo(String _id,
                    String meshid,
                    String name,
                    String mtype,
                    String desc,
                    String domain,
                    JSONObject links,
                    int consent,
                    int flags) {
        this._id = _id;
        this.meshid = meshid;
        this.name = name;
        this.mtype = mtype;
        this.desc = desc;
        this.domain = domain;
        this.links = links;
        this.consent = consent;
        this.flags = flags;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMeshid() {
        return meshid;
    }

    public void setMeshid(String meshid) {
        this.meshid = meshid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public JSONObject getLinks() {
        return links;
    }

    public void setLinks(JSONObject links) {
        this.links = links;
    }

    public int getConsent() {
        return consent;
    }

    public void setConsent(int consent) {
        this.consent = consent;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }
}
