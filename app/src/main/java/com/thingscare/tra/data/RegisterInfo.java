package com.thingscare.tra.data;

public class RegisterInfo {

    private String nodeid;

    public RegisterInfo() {}

    public RegisterInfo(String nodeid) {
        this.nodeid = nodeid;
    }

    public String getNodeid() {
        return nodeid;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }
}
